package s222222;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import diet.Food;
import diet.Menu;
import diet.Recipe;

public class TestClassButItWontCompileAnyways {
	
Food f;
	
	@Before
	public void setUp() {
		this.f = new Food();
	}
	
	@Test
	public void testR1_RawMaterials() {
		f.defineRawMaterial("Pasta", 200, 100, 50, 20);
		f.defineRawMaterial("Bread", 250, 70, 60, 15);
		f.defineRawMaterial("Zucchini", 60, 100, 20, 10);
		
		assertTrue(f.getRawMaterial("Pasta").getCalories() == 200);
		assertTrue(f.getRawMaterial("Pasta").getProteins() == 100);
		assertTrue(f.getRawMaterial("Pasta").getCarbs() == 50);
		assertTrue(f.getRawMaterial("Pasta").getFat() == 20);
		assertTrue(f.getRawMaterial("Pasta").per100g());
		
		assertTrue(f.getRawMaterial("Bread").getCalories() == 250);
		assertTrue(f.getRawMaterial("Bread").getProteins() == 70);
		assertTrue(f.getRawMaterial("Bread").getCarbs() == 60);
		assertTrue(f.getRawMaterial("Bread").getFat() == 15);
		assertTrue(f.getRawMaterial("Bread").per100g());
		
		assertTrue(f.getRawMaterial("Zucchini").getCalories() == 60);
		assertTrue(f.getRawMaterial("Zucchini").getProteins() == 100);
		assertTrue(f.getRawMaterial("Zucchini").getCarbs() == 20);
		assertTrue(f.getRawMaterial("Zucchini").getFat() == 10);
		assertTrue(f.getRawMaterial("Zucchini").per100g());
	}

	@Test
	public void testR2_products() {
		f.defineProduct("Nutella", 120, 10, 30, 70);
		f.defineProduct("Biscotti", 80, 20, 10, 30);
		f.defineProduct("Pizza", 100, 20, 70, 30);
		f.defineProduct("Coca-Cola", 90, 10, 30, 70);
		
		assertTrue(120 == f.getProduct("Nutella").getCalories());
		assertTrue(10 == f.getProduct("Nutella").getProteins());
		assertTrue(30 == f.getProduct("Nutella").getCarbs());
		assertTrue(70 == f.getProduct("Nutella").getFat());
		
		assertTrue(80 == f.getProduct("Biscotti").getCalories());
		assertTrue(20 == f.getProduct("Biscotti").getProteins());
		assertTrue(10 == f.getProduct("Biscotti").getCarbs());
		assertTrue(30 == f.getProduct("Biscotti").getFat());
		
		assertTrue(100 == f.getProduct("Pizza").getCalories());
		assertTrue(20 == f.getProduct("Pizza").getProteins());
		assertTrue(70 == f.getProduct("Pizza").getCarbs());
		assertTrue(30 == f.getProduct("Pizza").getFat());
		
		assertTrue(90 == f.getProduct("Coca-Cola").getCalories());
		assertTrue(10 == f.getProduct("Coca-Cola").getProteins());
		assertTrue(30 == f.getProduct("Coca-Cola").getCarbs());
		assertTrue(70 == f.getProduct("Coca-Cola").getFat());
	}
	
	@Test
	public void testR3_recipes() {
		f.defineRawMaterial("Pasta", 200, 100, 50, 20);
		f.defineRawMaterial("Pomodoro", 20, 10, 30, 50);
		
		Recipe r = f.createRecipe("Pasta al sugo");
		assertNotNull(f.getRecipe("Pasta al sugo"))
		
		r.addIngredient("Pasta", 80);
		r.addIngredient("Pomodoro", 10);
		assertEquals((0.8 * 200 + 0.1 * 20), r.getCalories() / 100 * 90, 0.001);
		assertEquals(0.8 * 100 + 0.1 * 10, r.getProteins() / 100 * 90, 0.001);
		assertEquals(0.8 * 50 + 0.1 * 30, r.getCarbs() / 100 * 90, 0.001);
		assertEquals(0.8 * 20 + 0.1 * 50, r.getFat() / 100 * 90, 0.001);
	}
	
	@Test
	public void testR4_menus() {
		f.defineRawMaterial("Zucchero", 400, 0, 100, 0);
	    f.defineRawMaterial("Mais", 70, 2.7, 12, 1.3);
	    f.defineRawMaterial("Pasta", 350, 12, 72.2, 1.5);
	    f.defineRawMaterial("Olio", 900, 0, 0, 100);
	    f.defineRawMaterial("Nutella", 530, 6.8, 56, 31);
		f.defineProduct("Cracker", 111, 2.6, 17.2, 3.5);
		f.defineProduct("Cornetto Cioccolato", 230, 3, 27, 11);
		f.defineProduct("Barretta Bueno", 122, 2, 10.6, 8);
		f.defineProduct("Mozzarella Light", 206, 25, 2, 11.25);

	    Recipe r = f.createRecipe("Pasta e nutella");
	    r.addIngredient("Pasta", 70);
	    r.addIngredient("Nutella", 30);
		Menu menu = f.createMenu("M1");
		

	    menu.addRecipe("Pasta e nutella", 50);
	    menu.addProduct("Cracker");

	    assertEquals("Wrong menu calories value",350 * 0.35 + 530 * 0.15 + 111, menu.getCalories(), 0.001);
	    assertEquals("Wrong menu proteins value",12 * 0.35 + 6.8 * 0.15 + 2.6, menu.getProteins(), 0.001);
	    assertEquals("Wrong menu carbs value",72.2 * 0.35 + 56 * 0.15 + 17.2, menu.getCarbs(), 0.001);
	    assertEquals("Wrong menu fat value",1.5 * 0.35 + 31 * 0.15 + 3.5, menu.getFat(), 0.001);
	}

}
